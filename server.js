const INDEX = './public/index.html';
const PORT = process.env.PORT || 8080;
const express = require('express');
const helmet = require('helmet');
const {
  Server
} = require('ws');

const server = express()
  .use(helmet({
    contentSecurityPolicy: {
      directives: {
        'default-src': ["'self'"],
        'script-src': ["'self'", "cdnjs.cloudflare.com"],
        'connect-src': ["'self'", "wss://beambms-global.herokuapp.com", "wss://beambms-global-staging.herokuapp.com"],
        'script-src-elem': ["'self'", "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js", "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.0/ScrollToPlugin.min.js"]
      }
    }
  }))
  .use(express.static('public', {
    setHeaders: function(res, path) {
      res.setHeader("X-XSS-Protection", "1; mode=block");
    }
  }))
  .use((req, res) => res.sendFile(INDEX, {
    root: __dirname
  }))
  .listen(PORT, () => console.log(`Listening on ${PORT}`));

const wss = new Server({
  server: server,
  maxPayload: 500 // bytes
});

var allRooms = [];
var roomMatch;

function createRoom(id, brand) {
  console.log('creating new room');
  var room = {
    id: id,
    brand: brand,
    clients: []
  };

  allRooms.push(room);

  return room;
}

wss.on('error', function(err) {
  console.log(err);
});

wss.on('connection', (ws, request, client) => {
  console.log(`Client connected`);

  ws.on('message', function(incomingData) {
    var data = JSON.parse(incomingData);

    roomMatch = allRooms.filter((room) => room.id === data.session_pin)[0];

    switch (data.payload) {
      case 'join-room':

        if (data.userType === 'rep') {
          if (!roomMatch) {
            console.log('no room match, creating room');
            roomMatch = createRoom(data.session_pin, data.brand);
          }
          console.log(roomMatch);

        } else if (data.userType === 'user') {

          if (!roomMatch) {
            // If a user submits an invalid code, send a no-match alert to the user

            var response = {
              payload: 'no-match'
            };

            ws.send(JSON.stringify(response));
            return;
          }

          var response = {
            payload: 'join-confirm',
            roomID: roomMatch.id,
            roomBrand: roomMatch.brand
          };

          wss.clients.forEach((client) => {
            client.send(JSON.stringify(response));
          });
        }

        // ws.id = data.clientID;
        // roomMatch.clients.push(ws);
        console.log('ALL ROOMS:', allRooms);
        console.log('ROOM MATCH:', roomMatch);
        break;

      case 'switch-brand':
        // Update roomMatch to the new brand
        roomMatch.brand = data.brand;
        console.log('ALL ROOMS:', allRooms);
        console.log('ROOM MATCH:', roomMatch);
        break;

      case 'end-session':
        // Clear room's clients and delete room from allRooms array
        roomMatch.clients = [];
        var targetIdx = allRooms.indexOf(roomMatch);
        allRooms.splice(targetIdx, 1);
        break;
      default:
        break;
    }
  });

  ws.on('close', function() {
    // console.log('Disconnected');
  });
});

// Handles WSS Upgrade Header Request
wss.on('upgrade', function upgrade(request, socket, head) {
  wss.handleUpgrade(request, socket, head, function done(ws) {
    wss.emit('connection', ws, request, client);
  });
});
