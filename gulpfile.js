// Packages Definitions
var fs = require('fs');
var path = require('path');
var del = require('del');
var gulp = require('gulp');
var gulpSequence = require('gulp-sequence');
var merge = require('merge-stream');
var browserSync = require('browser-sync').create();
var hogan = require('gulp-hogan');
var sass = require('gulp-sass');
var concatCss = require('gulp-concat-css');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');

// PATH DEFINITIONS
var SOURCE_PATH = './src';
var PUBLIC_PATH = './public';
var STYLE_PATH = SOURCE_PATH + '/style/';
var JS_PATH = SOURCE_PATH + '/js/';

gulp.task('serve', function(callback) {
  gulpSequence('build', serve)(callback)
})
gulp.task('build', function(callback) {
  gulpSequence('clean-build', 'process-html', 'copy-slides', 'process-css', 'process-js', 'clean-duplicate-files', browserSync.reload)(callback)
})
gulp.task('clean-build', cleanBuild);
gulp.task('process-html', processHTML);
gulp.task('process-css', processCSS);
gulp.task('process-js', processJS);
gulp.task('clean-duplicate-files', cleanDuplicateFiles);
gulp.task('copy-slides', copySlides);

function serve(callback) {
  gulp.watch(SOURCE_PATH + '/**/*', ['build'], browserSync.reload);
}

function processHTML() {
  return gulp.src('src/**/*.hogan', {}, '.html')
    .pipe(hogan(null, null, '.html'))
    .pipe(gulp.dest('public'))
}

function processJS() {
  return gulp.src(path.join(JS_PATH, '*.js'))
  .pipe(concat('bundle.js'))
  .pipe(gulp.dest(path.join(PUBLIC_PATH, 'js')));
}

function processCSS() {
  return gulp.src(path.join(STYLE_PATH, '**/*.scss'))
    .pipe(sass().on('error', sass.logError))
    .pipe(concatCss('bundle.css'))
    .pipe(autoprefixer())
    .pipe(gulp.dest(path.join(PUBLIC_PATH, 'style')));
}

function initBrowserSync() {
  browserSync.init({
    server: {
      baseDir: PUBLIC_PATH
    },
    open: true,
    notify: false
  });
}

function copySlides() {
  return gulp.src(SOURCE_PATH + '/**/*', {
    base: SOURCE_PATH
  })
  .pipe(gulp.dest(PUBLIC_PATH));
}

function cleanBuild() {
  return del([PUBLIC_PATH]);
}

function cleanDuplicateFiles() {
  return del([PUBLIC_PATH + '/**/*.scss', PUBLIC_PATH + '/**/*.js', '!' + PUBLIC_PATH + '/**/bundle.js', PUBLIC_PATH + '/**/*.hogan', PUBLIC_PATH + '/templates']);
}
