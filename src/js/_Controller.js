'use strict';

class Controller {
  init() {
    this.LOGIN_FORM = new LoginForm(this);
    this.SOCKET = new Socket(this);
    this.OVERLAY = new Overlay(this);
    this.sessionActive = false;

    // Make sure user is logged out by default on app start
    this.LOGIN_FORM.logOutSession();
  }

  closeActiveModal() {
    var activeModal = document.querySelector('.modal.visible');
    if (!activeModal) return;
    gsap.to([activeModal], {display: 'none', opacity: 0, duration: MODAL_HIDE_DURATION, ease: Power1.easeOut});
    this.OVERLAY.deactivate();
    activeModal.classList.remove('visible');
    this.HANDLER.enableScroll();
  }
}

var CONTROLLER = new Controller();
window.onload = function() {
  CONTROLLER.init();
};

function getIosVersion() {
  if (/iP(hone|od|ad)/.test(navigator.platform)) {
    var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
    return parseInt(v[1], 10);
  }
}

// var ver = getIosVersion();
// console.log(`ios version is ${ver}`);
