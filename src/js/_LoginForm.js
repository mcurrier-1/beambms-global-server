class LoginForm {
  constructor(controller) {
    this.controller = controller;
    this.form = document.getElementById('login-form');
    this.inputs = document.getElementsByClassName('pin-input');
    this.submit = document.getElementById('pin-submit-btn');

    this.pinCharMax = PIN_MAX_LENGTH;
    this.enteredCode = '';
    this.submitReady = false;
    this.deleteCounter;
    this.init();
  }

  init() {
    // Ensure empty inputs at start
    for (var i = 0; i < this.inputs.length; i++) {
      this.inputs[i].value = '';
      this.inputs[i].addEventListener('keyup', this.onInputChange.bind(this));
    }

    this.inputs[this.inputs.length - 1].addEventListener('keyup', this.stopInput.bind(this));
    this.inputs[this.inputs.length - 1].addEventListener('keydown', this.stopInput.bind(this));
    this.inputs[this.inputs.length - 1].addEventListener('textInput', this.stopInput.bind(this));
    this.submit.addEventListener('click', this.onButtonSubmit.bind(this));

    document.addEventListener('keydown', function(event) {
      this.target = event.srcElement || event.target;
      if (event.code === "Backspace" || event.keyCode === 8) {
        this.deleteCounter++;
        if (this.deleteCounter > 1) {
          this.previousEl = this.target;
          while (this.previousEl = this.previousEl.previousElementSibling) {
            if (this.previousEl == null) {
              break;
            } else if (this.previousEl.tagName.toLowerCase() === 'input') {
              this.previousEl.select();
              break;
            }
          }
        }
      } else if (event.code != "Backspace") {
        this.deleteCounter = 0;
      }
    });
  }

  // Android Input Bug Fix
  stopInput(event) {
    var target = event.srcElement || event.target;
    var inputMaxLength = parseInt(target.attributes['maxlength'].value, 10);
    var enteredLength = target.value.length;
    if ((enteredLength >= inputMaxLength) && (event.keyCode !== 8)) {
      target.value = target.value.slice(0, 1);
    }
  }

  onInputChange(event) {
    this.target = event.srcElement || event.target;
    this.inputMaxLength = parseInt(this.target.attributes['maxlength'].value, 10);
    this.enteredLength = this.target.value.length;
    // After character is entered, move to next input until max length is reached
    if (this.enteredLength === this.inputMaxLength) {
      this.nextEl = this.target;
      while (this.nextEl = this.nextEl.nextElementSibling) {
        if (this.nextEl == null) {
          break;
        } else if (this.nextEl.tagName.toLowerCase() === 'input') {
          this.nextEl.focus();
          break;
        }
      }
    }

    // Compile enteredCode from entered characters
    this.enteredCode = '';
    for (var i = 0; i < this.inputs.length; i++) {
      this.enteredCode += this.inputs[i].value;
    }

    // Disable submit button until code length
    if (this.enteredCode.length < this.pinCharMax) {
      this.submit.classList.add('disabled');
    } else {
      this.submit.classList.remove('disabled');
      this.submitReady = true;
    }
  }

  onButtonSubmit(event) {
    event.target.classList.add('disabled');

    // Android Input Bug Fix
    this.enteredCode = this.enteredCode.slice(0, this.pinCharMax);
    this.enteredCode = this.enteredCode.toUpperCase();
    this.submitEnteredCode();
  }

  submitEnteredCode() {
    console.log('user submitted code ' + this.enteredCode);
    // localStorage.setItem('globalUserPIN', this.enteredCode);
    this.controller.pin = this.enteredCode;
    this.logInSession();

    var that = this;
    setTimeout(function() {
      that.controller.SOCKET.joinRoom(that.enteredCode);
    }, 3000);

    this.exitLoginForm();
  }

  logInSession() {
    localStorage.setItem('globalLoggedIn', 'true');
  }

  logOutSession() {
    // Set local storage variables to initial status
    localStorage.setItem('globalLoggedIn', 'false');
    localStorage.setItem('globalUserPIN', '');
  }

  exitLoginForm() {
    gsap.to(this.form, {opacity: 0, duration: 0.3, ease: Power1.easeOut});
    gsap.to(this.form, {display: 'none', duration: 0.1, ease: Power0.easeNone, delay: 0.3});
    gsap.to('#standby', {opacity: 1, display: 'flex', duration: 0.75, ease: Power3.easeInOut, delay: 0.4});
  }
}
