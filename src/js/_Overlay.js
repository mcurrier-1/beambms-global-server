class Overlay {
  constructor(controller) {
    this.controller = controller;
    this.el = document.querySelector('#modal-overlay');
    this.active = false;
    this.init();
  }
  
  init() {
    var that = this;
    this.el.addEventListener('click', function() {
      that.controller.closeActiveModal();
    });
  }

  activate() {
    this.active = true;
    gsap.to([this.el], {display: 'block', opacity: 1, pointerEvents: 'all', duration: MODAL_SHOW_DURATION, delay: SCROLL_DELAY, ease: Power1.easeOut});
  }

  deactivate() {
    this.active = false;
    gsap.to([this.el], {display: 'none', opacity: 0, pointerEvents: 'none', duration: MODAL_HIDE_DURATION, ease: Power1.easeOut});
  }
}
