class Socket {
  constructor(controller) {
    this.controller = controller;
    // ********** PRODUCTION ********
    // this.socket = new ReconnectingWebSocket('wss://beambms-global.herokuapp.com/', [], {maxPayload: 500});
    // ********** STAGING ********
    this.socket = new ReconnectingWebSocket('wss://beambms-global-staging.herokuapp.com/', [], {maxPayload: 500});

    this.socket.timeoutInterval = RECONNECT_INTERVAL; //ms before attempting to reconnect
    this.loggedIn = false;
    this.brandUrls = {
      'reblozyl': 'https://beambms-reblozyl-staging.herokuapp.com/',
      'inrebic': 'https://beambms-mf-staging.herokuapp.com/',
      'onureg': 'https://beambms-onureg-staging.herokuapp.com/',
    }

    this.init();
  }

  init() {
    this.socket.onopen = this.startPing.bind(this);
    this.socket.onmessage = this.processMessage.bind(this);
    this.socket.onerror = function(error) {
      console.log(error);
    }

    this.socket.onclose = function(event) {
      console.log('Disconnected from WS server');
      clearInterval(pingInterval);
    }
  }

  startPing(socket) {
    console.log('Connected');
    this.startPingInterval(this.socket);
  }

  joinRoom(pin) {
    var joinReq = {
      payload: 'join-room',
      userType: 'user',
      session_pin: pin
    }

    console.log(joinReq);

    this.socket.send(JSON.stringify(joinReq));
  }

  processMessage(event) {
    var incomingData = JSON.parse(event.data);
    var incomingID = incomingData.roomID;
    this.loggedIn = localStorage.getItem('globalLoggedIn');
    console.log(incomingData);

    if (this.loggedIn === 'true') {
      this.handleMessage(incomingData);
    }
  }

  handleMessage(data) {

    if (data.payload === 'join-confirm') {
      // If the join-room request was successful, redirect user to matching brand site
      var siteMatch = this.brandUrls[data.roomBrand];
      // Append the current session PIN to the end of the url as a query string
      siteMatch = siteMatch + '?pin=' + this.controller.pin;
      console.log(siteMatch);
      window.open(siteMatch, '_self');

    } else if (data.payload === 'no-match') {
      // Alert user that submitted PIN was invalid then redirect back to PIN entry screen
      alert('No session match found, please try another code.');
      location.reload();

    } else if (data.payload === 'finish-session') {
      // If received data command is finish-session, begin finishSession process
      this.controller.HANDLER.finishSession();
      return;
    }
  }

  pingServer(webSocket) {
    this.pingData = {
      type: 'server-ping',
      payload: 'server ping'
    };

    // console.log('sending server ping');
    webSocket.send(JSON.stringify(this.pingData));
  }

  startPingInterval(webSocket) {
    this.pingInterval = setInterval(this.pingServer.bind(this, webSocket), PING_INT);
  }
}
