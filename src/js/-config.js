// CONFIG CONSTANTS
const PING_INT = 2000; // milliseconds
const RECONNECT_INTERVAL = 1100; // milliseconds
const PIN_MAX_LENGTH = 4; // characters
const MODAL_SHOW_DURATION = 0.4; // seconds
const MODAL_HIDE_DURATION = 0.2; // seconds
