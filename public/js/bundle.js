// CONFIG CONSTANTS
const PING_INT = 2000; // milliseconds
const RECONNECT_INTERVAL = 1100; // milliseconds
const PIN_MAX_LENGTH = 4; // characters
const MODAL_SHOW_DURATION = 0.4; // seconds
const MODAL_HIDE_DURATION = 0.2; // seconds

'use strict';

class Controller {
  init() {
    this.LOGIN_FORM = new LoginForm(this);
    this.SOCKET = new Socket(this);
    this.OVERLAY = new Overlay(this);
    this.sessionActive = false;

    // Make sure user is logged out by default on app start
    this.LOGIN_FORM.logOutSession();
  }

  closeActiveModal() {
    var activeModal = document.querySelector('.modal.visible');
    if (!activeModal) return;
    gsap.to([activeModal], {display: 'none', opacity: 0, duration: MODAL_HIDE_DURATION, ease: Power1.easeOut});
    this.OVERLAY.deactivate();
    activeModal.classList.remove('visible');
    this.HANDLER.enableScroll();
  }
}

var CONTROLLER = new Controller();
window.onload = function() {
  CONTROLLER.init();
};

function getIosVersion() {
  if (/iP(hone|od|ad)/.test(navigator.platform)) {
    var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
    return parseInt(v[1], 10);
  }
}

// var ver = getIosVersion();
// console.log(`ios version is ${ver}`);

class LoginForm {
  constructor(controller) {
    this.controller = controller;
    this.form = document.getElementById('login-form');
    this.inputs = document.getElementsByClassName('pin-input');
    this.submit = document.getElementById('pin-submit-btn');

    this.pinCharMax = PIN_MAX_LENGTH;
    this.enteredCode = '';
    this.submitReady = false;
    this.deleteCounter;
    this.init();
  }

  init() {
    // Ensure empty inputs at start
    for (var i = 0; i < this.inputs.length; i++) {
      this.inputs[i].value = '';
      this.inputs[i].addEventListener('keyup', this.onInputChange.bind(this));
    }

    this.inputs[this.inputs.length - 1].addEventListener('keyup', this.stopInput.bind(this));
    this.inputs[this.inputs.length - 1].addEventListener('keydown', this.stopInput.bind(this));
    this.inputs[this.inputs.length - 1].addEventListener('textInput', this.stopInput.bind(this));
    this.submit.addEventListener('click', this.onButtonSubmit.bind(this));

    document.addEventListener('keydown', function(event) {
      this.target = event.srcElement || event.target;
      if (event.code === "Backspace" || event.keyCode === 8) {
        this.deleteCounter++;
        if (this.deleteCounter > 1) {
          this.previousEl = this.target;
          while (this.previousEl = this.previousEl.previousElementSibling) {
            if (this.previousEl == null) {
              break;
            } else if (this.previousEl.tagName.toLowerCase() === 'input') {
              this.previousEl.select();
              break;
            }
          }
        }
      } else if (event.code != "Backspace") {
        this.deleteCounter = 0;
      }
    });
  }

  // Android Input Bug Fix
  stopInput(event) {
    var target = event.srcElement || event.target;
    var inputMaxLength = parseInt(target.attributes['maxlength'].value, 10);
    var enteredLength = target.value.length;
    if ((enteredLength >= inputMaxLength) && (event.keyCode !== 8)) {
      target.value = target.value.slice(0, 1);
    }
  }

  onInputChange(event) {
    this.target = event.srcElement || event.target;
    this.inputMaxLength = parseInt(this.target.attributes['maxlength'].value, 10);
    this.enteredLength = this.target.value.length;
    // After character is entered, move to next input until max length is reached
    if (this.enteredLength === this.inputMaxLength) {
      this.nextEl = this.target;
      while (this.nextEl = this.nextEl.nextElementSibling) {
        if (this.nextEl == null) {
          break;
        } else if (this.nextEl.tagName.toLowerCase() === 'input') {
          this.nextEl.focus();
          break;
        }
      }
    }

    // Compile enteredCode from entered characters
    this.enteredCode = '';
    for (var i = 0; i < this.inputs.length; i++) {
      this.enteredCode += this.inputs[i].value;
    }

    // Disable submit button until code length
    if (this.enteredCode.length < this.pinCharMax) {
      this.submit.classList.add('disabled');
    } else {
      this.submit.classList.remove('disabled');
      this.submitReady = true;
    }
  }

  onButtonSubmit(event) {
    event.target.classList.add('disabled');

    // Android Input Bug Fix
    this.enteredCode = this.enteredCode.slice(0, this.pinCharMax);
    this.enteredCode = this.enteredCode.toUpperCase();
    this.submitEnteredCode();
  }

  submitEnteredCode() {
    console.log('user submitted code ' + this.enteredCode);
    // localStorage.setItem('globalUserPIN', this.enteredCode);
    this.controller.pin = this.enteredCode;
    this.logInSession();

    var that = this;
    setTimeout(function() {
      that.controller.SOCKET.joinRoom(that.enteredCode);
    }, 3000);

    this.exitLoginForm();
  }

  logInSession() {
    localStorage.setItem('globalLoggedIn', 'true');
  }

  logOutSession() {
    // Set local storage variables to initial status
    localStorage.setItem('globalLoggedIn', 'false');
    localStorage.setItem('globalUserPIN', '');
  }

  exitLoginForm() {
    gsap.to(this.form, {opacity: 0, duration: 0.3, ease: Power1.easeOut});
    gsap.to(this.form, {display: 'none', duration: 0.1, ease: Power0.easeNone, delay: 0.3});
    gsap.to('#standby', {opacity: 1, display: 'flex', duration: 0.75, ease: Power3.easeInOut, delay: 0.4});
  }
}

class Overlay {
  constructor(controller) {
    this.controller = controller;
    this.el = document.querySelector('#modal-overlay');
    this.active = false;
    this.init();
  }
  
  init() {
    var that = this;
    this.el.addEventListener('click', function() {
      that.controller.closeActiveModal();
    });
  }

  activate() {
    this.active = true;
    gsap.to([this.el], {display: 'block', opacity: 1, pointerEvents: 'all', duration: MODAL_SHOW_DURATION, delay: SCROLL_DELAY, ease: Power1.easeOut});
  }

  deactivate() {
    this.active = false;
    gsap.to([this.el], {display: 'none', opacity: 0, pointerEvents: 'none', duration: MODAL_HIDE_DURATION, ease: Power1.easeOut});
  }
}

class Socket {
  constructor(controller) {
    this.controller = controller;
    // ********** PRODUCTION ********
    // this.socket = new ReconnectingWebSocket('wss://beambms-global.herokuapp.com/', [], {maxPayload: 500});
    // ********** STAGING ********
    this.socket = new ReconnectingWebSocket('wss://beambms-global-staging.herokuapp.com/', [], {maxPayload: 500});

    this.socket.timeoutInterval = RECONNECT_INTERVAL; //ms before attempting to reconnect
    this.loggedIn = false;
    this.brandUrls = {
      'reblozyl': 'https://beambms-reblozyl-staging.herokuapp.com/',
      'inrebic': 'https://beambms-mf-staging.herokuapp.com/',
      'onureg': 'https://beambms-onureg-staging.herokuapp.com/',
    }

    this.init();
  }

  init() {
    this.socket.onopen = this.startPing.bind(this);
    this.socket.onmessage = this.processMessage.bind(this);
    this.socket.onerror = function(error) {
      console.log(error);
    }

    this.socket.onclose = function(event) {
      console.log('Disconnected from WS server');
      clearInterval(pingInterval);
    }
  }

  startPing(socket) {
    console.log('Connected');
    this.startPingInterval(this.socket);
  }

  joinRoom(pin) {
    var joinReq = {
      payload: 'join-room',
      userType: 'user',
      session_pin: pin
    }

    console.log(joinReq);

    this.socket.send(JSON.stringify(joinReq));
  }

  processMessage(event) {
    var incomingData = JSON.parse(event.data);
    var incomingID = incomingData.roomID;
    this.loggedIn = localStorage.getItem('globalLoggedIn');
    console.log(incomingData);

    if (this.loggedIn === 'true') {
      this.handleMessage(incomingData);
    }
  }

  handleMessage(data) {

    if (data.payload === 'join-confirm') {
      // If the join-room request was successful, redirect user to matching brand site
      var siteMatch = this.brandUrls[data.roomBrand];
      // Append the current session PIN to the end of the url as a query string
      siteMatch = siteMatch + '?pin=' + this.controller.pin;
      console.log(siteMatch);
      window.open(siteMatch, '_self');

    } else if (data.payload === 'no-match') {
      // Alert user that submitted PIN was invalid then redirect back to PIN entry screen
      alert('No session match found, please try another code.');
      location.reload();

    } else if (data.payload === 'finish-session') {
      // If received data command is finish-session, begin finishSession process
      this.controller.HANDLER.finishSession();
      return;
    }
  }

  pingServer(webSocket) {
    this.pingData = {
      type: 'server-ping',
      payload: 'server ping'
    };

    // console.log('sending server ping');
    webSocket.send(JSON.stringify(this.pingData));
  }

  startPingInterval(webSocket) {
    this.pingInterval = setInterval(this.pingServer.bind(this, webSocket), PING_INT);
  }
}

!function(a,b){"function"==typeof define&&define.amd?define([],b):"undefined"!=typeof module&&module.exports?module.exports=b():a.ReconnectingWebSocket=b()}(this,function(){function a(b,c,d){function l(a,b){var c=document.createEvent("CustomEvent");return c.initCustomEvent(a,!1,!1,b),c}var e={debug:!1,automaticOpen:!0,reconnectInterval:1e3,maxReconnectInterval:3e4,reconnectDecay:1.5,timeoutInterval:2e3};d||(d={});for(var f in e)this[f]="undefined"!=typeof d[f]?d[f]:e[f];this.url=b,this.reconnectAttempts=0,this.readyState=WebSocket.CONNECTING,this.protocol=null;var h,g=this,i=!1,j=!1,k=document.createElement("div");k.addEventListener("open",function(a){g.onopen(a)}),k.addEventListener("close",function(a){g.onclose(a)}),k.addEventListener("connecting",function(a){g.onconnecting(a)}),k.addEventListener("message",function(a){g.onmessage(a)}),k.addEventListener("error",function(a){g.onerror(a)}),this.addEventListener=k.addEventListener.bind(k),this.removeEventListener=k.removeEventListener.bind(k),this.dispatchEvent=k.dispatchEvent.bind(k),this.open=function(b){h=new WebSocket(g.url,c||[]),b||k.dispatchEvent(l("connecting")),(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","attempt-connect",g.url);var d=h,e=setTimeout(function(){(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","connection-timeout",g.url),j=!0,d.close(),j=!1},g.timeoutInterval);h.onopen=function(){clearTimeout(e),(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onopen",g.url),g.protocol=h.protocol,g.readyState=WebSocket.OPEN,g.reconnectAttempts=0;var d=l("open");d.isReconnect=b,b=!1,k.dispatchEvent(d)},h.onclose=function(c){if(clearTimeout(e),h=null,i)g.readyState=WebSocket.CLOSED,k.dispatchEvent(l("close"));else{g.readyState=WebSocket.CONNECTING;var d=l("connecting");d.code=c.code,d.reason=c.reason,d.wasClean=c.wasClean,k.dispatchEvent(d),b||j||((g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onclose",g.url),k.dispatchEvent(l("close")));var e=g.reconnectInterval*Math.pow(g.reconnectDecay,g.reconnectAttempts);setTimeout(function(){g.reconnectAttempts++,g.open(!0)},e>g.maxReconnectInterval?g.maxReconnectInterval:e)}},h.onmessage=function(b){(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onmessage",g.url,b.data);var c=l("message");c.data=b.data,k.dispatchEvent(c)},h.onerror=function(b){(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onerror",g.url,b),k.dispatchEvent(l("error"))}},1==this.automaticOpen&&this.open(!1),this.send=function(b){if(h)return(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","send",g.url,b),h.send(b);throw"INVALID_STATE_ERR : Pausing to reconnect websocket"},this.close=function(a,b){"undefined"==typeof a&&(a=1e3),i=!0,h&&h.close(a,b)},this.refresh=function(){h&&h.close()}}return a.prototype.onopen=function(){},a.prototype.onclose=function(){},a.prototype.onconnecting=function(){},a.prototype.onmessage=function(){},a.prototype.onerror=function(){},a.debugAll=!1,a.CONNECTING=WebSocket.CONNECTING,a.OPEN=WebSocket.OPEN,a.CLOSING=WebSocket.CLOSING,a.CLOSED=WebSocket.CLOSED,a});
